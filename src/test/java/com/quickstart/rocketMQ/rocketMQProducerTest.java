package com.quickstart.rocketMQ;

import com.quickstart.entity.MqMsg;
import com.quickstart.service.RocketMqService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

/**
 * @author cloud
 * @version 1.0
 * @description: TODO
 * @date 2023/12/4 16:50
 */
@SpringBootTest
public class rocketMQProducerTest {

    @Autowired
    private RocketMqService rocketMqService;

    @Test
    public void sendMsg(){
        MqMsg build = MqMsg.builder().tags("name")
                .topic("MY_TOPIC")
                .content("测试tag")
                .hashKey("100")
                .build();
        rocketMqService.send(build);
    }

}
