package com.quickstart.mongodb;

import com.quickstart.WebApplication;
import com.quickstart.entity.MongoUser;
import com.quickstart.service.UserService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

/**
 * @author muguozheng
 * @version 1.0.0
 * @createTime 2022/8/30 16:08
 * @description mongo测试
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = WebApplication.class)
public class MongoUserTest {
    @Autowired
    private UserService userService;
    @Autowired
    private MongoTemplate mongoTemplate;

    @Test
    public void insertUserTest() {
        MongoUser mongoUser = new MongoUser();
        mongoUser.setId("2020450115");
        mongoUser.setUsername("蒂法");
        mongoUser.setPassword("20123456");
        mongoUser.setAge(21);
        mongoUser.setGender(0);
        mongoUser.setCity("南京市");
        mongoUser.setCreateTime(LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")));
        userService.saveUser(mongoUser);
    }
    @Test
    public void updateTest() {
        MongoUser mongoUser = new MongoUser();
        mongoUser.setId("522345");
        mongoUser.setUsername("132");
        mongoUser.setPassword("999999");
        Query query = new Query();
        query.addCriteria(Criteria.where("_id").is(mongoUser.getId()).and("username").is(mongoUser.getUsername()));
        Update update = new Update();
        update.set("password", mongoUser.getPassword());
        mongoTemplate.updateFirst(query, update, "user");
    }
    @Test
    public void deleteTest() {
        Query query = new Query(Criteria.where("_id").is("123"));
        mongoTemplate.remove(query,"user");
    }

    @Test
    public void findTest() {
        Query query = new Query(Criteria.where("_id").is("2020450114"));
        System.out.println(mongoTemplate.findOne(query, MongoUser.class));
    }
    @Test
    public void queryListTest() {
        MongoUser mongoUser = new MongoUser();
        mongoUser.setCity("北京市");
        mongoUser.setAge(19);

        System.out.println(userService.queryList(mongoUser));
    }
}
