package com.quickstart.jpa;

import com.querydsl.jpa.impl.JPAQueryFactory;
import com.quickstart.WebApplication;
import com.quickstart.entity.QModel.QArticle;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

/**
 * @author cloud
 * @version 1.0
 * @description: TODO
 * @date 2023/12/18 13:50
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = WebApplication.class )
public class QueryDSLTest {

    @Autowired
    private JPAQueryFactory queryFactory;

    @Test
    @Transactional(rollbackFor= Exception.class )
    public void testUpdate() {
        QArticle article = QArticle.article;
        System.out.println(queryFactory.update(article)
                .set(article.content, "qsl 测试")
                .where(article.id.eq(2)).execute());
    }

}
