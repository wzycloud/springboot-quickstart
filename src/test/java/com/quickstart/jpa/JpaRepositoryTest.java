package com.quickstart.jpa;

import com.quickstart.WebApplication;
import com.quickstart.entity.model.Article;
import com.quickstart.repository.ArticleRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Collections;
import java.util.List;
import java.util.Optional;

/**
 * @author cloud
 * @version 1.0
 * @description: TODO
 * @date 2023/12/15 16:42
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = WebApplication.class )
public class JpaRepositoryTest {
    @Autowired
    public ArticleRepository articleRepository;

    @Test // 测试查询全部记录
    public void testUpdate() {
        Article article = Article.builder()
                .title("batch save标题")
                .content("Batch save内容")
                .build();
        List<Article> list = Collections.singletonList(article);
        // 查询全部文章记录
        List<Article> articles = articleRepository.saveAll(list);
        System.out.println(articles);

    }
    @Test // 测试查询全部记录
    public void testDelete() {
        Article article = Article.builder()
                .title("batch save标题")
                .content("Batch save内容")
                .build();
        List<Article> list = Collections.singletonList(article);
        // 查询全部文章记录
        List<Article> articles = articleRepository.saveAll(list);
        System.out.println(articles);

    }
    @Test // 测试查询全部记录
    public void testBatchSave() {
        Article article = Article.builder()
                .title("batch save标题")
                .content("Batch save内容")
                .build();
        List<Article> list = Collections.singletonList(article);
        // 查询全部文章记录
        List<Article> articles = articleRepository.saveAll(list);
        System.out.println(articles);

    }
    @Test // 测试查询全部记录
    public void testSave() {
        Article article = Article.builder()
                .id(3)
                .title("update标题")
                .content("update内容")
                .build();
        // 查询全部文章记录
        Article save = articleRepository.save(article);
        System.out.println(save);

    }
    @Test // 测试查询全部记录
    public void testFindById() {
        // 查询全部文章记录
        Optional<Article> byId = articleRepository.findById(1);
        Article article = byId.orElse(new Article());
        System.out.println(article);
        byId.ifPresent((System.out::println));
    }
    @Test // 测试查询全部记录
    public void testFindAll() {
        // 查询全部文章记录
        List<Article> articles = articleRepository.findAll();
        // 遍历输出文章列表
        articles.forEach(System.out::println);
    }

}
