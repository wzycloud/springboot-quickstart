package com.quickstart.minio;

import com.quickstart.WebApplication;
import com.quickstart.config.MinioProperties;
import io.minio.DownloadObjectArgs;
import io.minio.GetObjectArgs;
import io.minio.MakeBucketArgs;
import io.minio.MinioClient;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.io.FileOutputStream;
import java.io.InputStream;

/**
 * @author muguozheng
 * @version 1.0.0
 * @createTime 2022/8/30 16:08
 * @description minio测试
 */
@Slf4j
@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = WebApplication.class)
public class MinioTest {

    @Autowired
    private MinioHandler minioHandler;
    @Autowired
    private MinioClient minioClient;
    @Autowired
    private  MinioProperties minioProperties;

    @Test
    public void existBucket() {
        boolean wzy = minioHandler.existBucket("wzy");
        System.out.println(wzy);
    }
    @Test
    public void createdBucket() {
        try {
            MakeBucketArgs test = MakeBucketArgs.builder().bucket("test").build();
            minioClient.makeBucket(test);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
    @Test
    public void downloadTest() {
        String bucketName = minioProperties.getBucketName(); // 存储桶名称
        //String objectName = "Jenkins资料.zip.netdisk.p.downloading"; // 文件在存储桶中的路径
        String objectName = "test.txt"; // 文件在存储桶中的路径
        //String localFilePath = "D:\\test\\Jenkins资料.zip.netdisk.p.downloading"; // 下载到本地的文件路径
        String localFilePath = "D:\\"; // 下载到本地的文件路径

        try {
            // 下载文件
            downloadBigFile(minioClient, bucketName, objectName, localFilePath);
            System.out.println("文件下载成功！");
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
    private  void downloadBigFile(MinioClient minioClient, String bucketName, String objectName, String localFilePath) throws Exception{
        // 获取文件流
        DownloadObjectArgs build = DownloadObjectArgs.builder()
                .bucket(bucketName)
                .object(objectName)
                .filename(localFilePath).build();
        minioClient.downloadObject(build);

    }

    private  void downloadFile(MinioClient minioClient, String bucketName, String objectName, String localFilePath) throws Exception{
        // 获取文件流
        GetObjectArgs build = GetObjectArgs.builder().bucket(bucketName).object(objectName).build();
        try (InputStream inputStream = minioClient.getObject(build)) {
            // 将文件流写入本地文件
            try (FileOutputStream outputStream = new FileOutputStream(localFilePath)) {
                byte[] buffer = new byte[1024*10];
                int bytesRead;
                while ((bytesRead = inputStream.read(buffer, 0, buffer.length)) >= 0) {
                    outputStream.write(buffer, 0, bytesRead);
                }
            }
        }
    }
}