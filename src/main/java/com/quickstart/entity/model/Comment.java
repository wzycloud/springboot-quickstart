package com.quickstart.entity.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;

/**
 * 功能：评论实体类
 * 日期：2023年06月13日
 * 作者：梁辰兴
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Entity(name = "t_comment")
public class Comment implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;
    @Column(name = "content")
    private String content;
    @Column(name = "author")
    private String author;
    @Column(name = "a_id")
    private Integer aId;
}
