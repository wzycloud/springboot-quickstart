package com.quickstart.entity.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

/**
 * 功能：文章实体类
 * 日期：2023年06月13日
 * 作者：梁辰兴
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Entity(name = "t_article")
public class Article implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;
    @Column(name = "title")
    private String title;
    @Column(name = "content")
    private String content;
    // 查询时将子表一并查询出来
    @OneToMany(fetch = FetchType.EAGER) // FetchType.LAZY 懒加载
    @JoinTable(name = "t_comment", joinColumns = {@JoinColumn(name = "a_id")},
            inverseJoinColumns = {@JoinColumn(name = "id")})
    private List<Comment> commentList;
}
