package com.quickstart.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Builder
@AllArgsConstructor
@NoArgsConstructor
@Data
public class MqMsg {
    /**
     * 一级消息：消息topic
     */
    private String topic;
    /**
     * 二级消息：消息topic对应的tags
     */
    private String tags;
    /**
     * 消息内容
     */
    private Object content;

    /**
     * 顺序hashkey
     */
    private String hashKey;
}