package com.quickstart.aspect;

import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.stereotype.Component;

/**
 * @author cloud
 * @version 1.0
 * @description: TODO
 * @date 2023/12/12 17:04
 */
@Aspect
@Component
@Slf4j
public class TimeConsumingAspect {
    @Pointcut("@annotation(com.quickstart.annotation.TimeConsuming)")
    public void pointcut() {}

    /**
     * 环绕增强，相当于MethodInterceptor
     */
    @Around("pointcut()")
    public Object around(ProceedingJoinPoint joinPoint) throws Throwable {
        long start = System.currentTimeMillis();
        Object proceed = joinPoint.proceed();
        long end = System.currentTimeMillis();
        //System.out.println("方法耗时==>"+ (end - start));
        log.info("方法耗时{}",(end-start));
        return proceed;
    }

}
