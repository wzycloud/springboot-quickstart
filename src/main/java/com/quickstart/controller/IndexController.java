package com.quickstart.controller;

import com.quickstart.annotation.CustomLog;
import com.quickstart.annotation.TimeConsuming;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author ding
 */
@RestController
public class IndexController {

    /**
     * 基础web
     */
    @GetMapping("/")
    @CustomLog("基础web")
    @TimeConsuming
    public String test(String msg) {
        return "欢迎使用 springboot-cli ！";
    }

}
