package com.quickstart.mq.rocketmq;

import org.apache.rocketmq.spring.annotation.RocketMQMessageListener;
import org.apache.rocketmq.spring.core.RocketMQListener;
import org.springframework.stereotype.Component;

/**
 * 泛型可选
 */
@Component
@RocketMQMessageListener(consumerGroup = "MY_CONSUMER_GROUP", topic = "MY_TOPIC")
public class RocketMQConsumer implements RocketMQListener<String> {

    @Override
    public void onMessage(String message) {
        System.out.println("Received message : " + message);
    }
}
