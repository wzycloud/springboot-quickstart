package com.quickstart.repository;


import com.quickstart.entity.model.Article;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * 功能：文章仓库接口
 * 日期：2023年06月13日
 * 作者：梁辰兴
 */
@Repository
public interface ArticleRepository extends JpaRepository<Article, Integer> {
}
