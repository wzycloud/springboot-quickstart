package com.quickstart.service;


import com.quickstart.entity.MongoUser;

import java.util.List;

/**
 * @author muguozheng
 * @version 1.0.0
 * @createTime 2022/8/30 15:58
 * @description 服务层
 */
public interface UserService {
    void saveUser(MongoUser mongoUser);

    List<MongoUser> queryList(MongoUser mongoUser);
}
