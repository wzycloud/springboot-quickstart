package com.quickstart.service.impl;

import com.quickstart.dao.UserMongoDbDao;
import com.quickstart.entity.MongoUser;
import com.quickstart.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author muguozheng
 * @version 1.0.0
 * @createTime 2022/8/30 15:59
 * @description mongo测试类
 */
@Service
public class UserServiceImpl implements UserService {
    @Autowired
    private UserMongoDbDao userMongoDbDao;

    @Override
    public void saveUser(MongoUser mongoUser) {
        userMongoDbDao.save(mongoUser);
    }

    @Override
    public List<MongoUser> queryList(MongoUser mongoUser) {
        return userMongoDbDao.queryList(mongoUser);
    }
}
