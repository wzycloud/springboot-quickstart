package com.quickstart.service.impl;

import com.quickstart.entity.MqMsg;
import com.quickstart.service.RocketMqService;
import lombok.extern.slf4j.Slf4j;
import org.apache.rocketmq.client.producer.SendCallback;
import org.apache.rocketmq.client.producer.SendResult;
import org.apache.rocketmq.client.producer.SendStatus;
import org.apache.rocketmq.spring.core.RocketMQTemplate;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
@Slf4j
@Service
public class RocketMqServiceImpl implements RocketMqService {
    @Resource
    private RocketMQTemplate rocketMQTemplate;
    @Override
    public void send(MqMsg mqMsg) {
        log.info("send发送消息到mqMsg={}", mqMsg);
        rocketMQTemplate.send(mqMsg.getTopic() + ":" + mqMsg.getTags(), MessageBuilder.withPayload(mqMsg.getContent()).build());
    }
    @Override
    public void asyncSend(MqMsg mqMsg) {
        log.info("asyncSend发送消息到mqMsg={}", mqMsg);
        rocketMQTemplate.asyncSend(mqMsg.getTopic() + ":" + mqMsg.getTags(), mqMsg.getContent(), new SendCallback() {
                    @Override
                    public void onSuccess(SendResult sendResult) {
                        // 成功不做日志记录或处理
                        SendStatus sendStatus = sendResult.getSendStatus();
                        System.out.println(sendStatus.name());
                        log.info("mqMsg={}消息发送成功", sendStatus.name());

                    }
                    @Override
                    public void onException(Throwable throwable) {
                        throwable.printStackTrace();
                        log.info("mqMsg={}消息发送失败", mqMsg);
                    }
                },1000);
    }
    @Override
    public void syncSendOrderly(MqMsg mqMsg) {
        log.info("syncSendOrderly发送消息到mqMsg={}", mqMsg);
        rocketMQTemplate.sendOneWay(mqMsg.getTopic() + ":" + mqMsg.getTags(), mqMsg.getContent());
    }


    @Override
    public void syncSendOrderlyNew(MqMsg mqMsg) {
        log.info("syncSendOrderly发送消息到mqMsg={}", mqMsg);
        rocketMQTemplate.syncSendOrderly(mqMsg.getTopic() + ":" + mqMsg.getTags(), mqMsg.getContent(), mqMsg.getHashKey());
    }
}